%top {
	char buf[64000];
}

%option noyywrap

NUM	[0-9]
HEX     [0-9A-Fa-f]


%x	COMMENT
%x	STRING

%%

{NUM}{2,8} 	{ printf("Number : %d\n", atoi(yytext));}
{NUM}{9,}  	{ printf("Long number %s\n", yytext); }
"0x"{HEX}{1,8}	{ printf("Hex number %s\n", &yytext[2]); }
a    		{ printf("A"); }
"/*"[.\n]*"*/"  { ; }
. 		;
"/*"		{ BEGIN(COMMENT); } //enter comment state
<COMMENT>"*/" 	{ BEGIN(INITIAL); } //leave coment state
<COMMENT>.	{ ; } // ignore anything else in comment state
"\""            { memset(buf, 0, 64000); BEGIN(STRING); }
<STRING>"\\n"	{ buf[strlen(buf)] = '\n'; }
<STRING>"\\\""	{ buf[strlen(buf)] = '\"';  }
<STRING>"\""	{ printf("STRING: %s", buf); BEGIN(INITIAL); }
<STRING>.	{ buf[strlen(buf)] = *yytext; }
\n		;

%%


int main(void){
	return yylex();
}

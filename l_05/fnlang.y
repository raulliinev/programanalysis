%{
#include "fnlang.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdarg.h>
void yyerror(const char *);
extern int yylex(void);
extern FILE *yyin;
extern int yylineno;

int varCount;
t_varNode **variables;

int fnCount;
t_function **functions;

t_node *block(t_node *a, t_node *b);
t_node *oper(int type, int n, ...);
t_node *intToNode(int val);
t_node *strToNode(char *s);
void addFn(t_varNode *var, int parCount, t_node *body);
t_node *varToFnCall(t_varNode *var);
t_node *varToNode(t_varNode *var);
t_node *nodeToParam(t_node *a);
t_node *moreParam(t_node *a, t_node *b);
%}

%union{
	int val;
	char *s;
	t_node *node;
	t_varNode *var;
}

%error-verbose
%token WHILE READ PRINT IF ELSE ENDIF FUNCTION
%token <val> INT
%token <s> STR
%token <var> VAR
%type <node> stmts stmt exp exps
%type <val> params

%left '+' '-'
%left '*' '/'
%right NEG
%%
prog	: fn					{ ; }
	| prog fn				{ ; }
	;
fn	: FUNCTION VAR '(' params ')' stmt	{ addFn($2, $4, $6); }
	;
params	: 					{ $$ = 0; }
	| VAR					{ $$ = 1; }
	| params ',' VAR			{ $$ = $1 + 1; }
	;
stmts	: stmt					{ $$ = $1; }
	| stmts stmt				{ $$ = block($1, $2); }
	;
stmt	: VAR '=' exp ';'		{ $$ = oper('=', 2, varToNode($1), $3); }
	| PRINT exp ';'			{ $$ = oper(PRINT, 1, $2); }
	| PRINT STR ';'			{ $$ = oper(PRINT, 1, strToNode($2)); }
	| READ VAR ';'			{ $$ = oper(READ, 1, varToNode($2)); }
	| WHILE exp stmt		{ $$ = oper(WHILE, 2, $2, $3); }
	| '{' stmts '}'			{ $$ = $2; }
	| IF exp stmt ENDIF		{ $$ = oper(IF, 2, $2, $3); }
	| IF exp stmt ELSE stmt ENDIF	{ $$ = oper(ELSE, 3, $2, $3, $5); }
	;
exp	: INT			{ $$ = intToNode($1); } 
	| VAR			{ $$ = varToNode($1); } //we can't create new one every time
	| exp '+' exp 		{ $$ = oper('+', 2, $1, $3); }
	| exp '-' exp 		{ $$ = oper('-', 2, $1, $3); }
	| exp '/' exp 		{ $$ = oper('/', 2, $1, $3); }
	| exp '*' exp 		{ $$ = oper('*', 2, $1, $3); }
	| '(' exp ')'		{ $$ = $2; }
	| '-' INT %prec NEG	{ $$ = oper(NEG, 1, intToNode($2)); }
	| VAR '(' exps ')'	{ $$ = oper(FUNCTION, 2, varToFnCall($1), $3); }
	;
exps	: 			{ $$ = nodeToParam(NULL); }
	| exp			{ $$ = nodeToParam($1); }
	| exps ',' exp		{ $$ = moreParam($1, $3); }
	;
%%
void init(void){
	varCount = 0;
	variables = NULL;
}
t_varNode *newVar(char *name){
	t_varNode *v = malloc(sizeof(t_varNode));
	assert(v != NULL);
	v->name = strdup(name);
	v->idx = varCount++;
	variables = realloc(variables, varCount * sizeof(void*));
	assert(variables != NULL);
	variables[varCount - 1] = v;
	return v;
}
t_varNode *strToVar(char *name){
	int i;
	for(i = 0; i < varCount; i++)
		if(strcasecmp(variables[i]->name, name) == 0){
			return variables[i];
		}	
	return newVar(name);
}

t_node *block(t_node *a, t_node *b){ 
	if(a == NULL && b != NULL)
		return b;
	if(a != NULL && b == NULL)
		return a;
	if(a == NULL && b == NULL)
		return NULL;
	if(a->type == tBlock){
		a->block.n++;
		a->block.statements = realloc(a->block.statements, a->block.n * sizeof(void*));
		assert(a->block.statements != NULL);
		a->block.statements[a->block.n - 1] = b;
		return a;
	}else{
		t_node *p = malloc(sizeof(t_node));
		assert(p != NULL);
		p->type = tBlock;
		p->block.n = 2;
		p->block.statements = malloc(2 * sizeof(void*));		
		assert(p->block.statements != NULL);
		p->block.statements[0] = a;
		p->block.statements[1] = b;
		return p;
	}
}

t_node *oper(int type, int n, ...){ 
	va_list args;
	t_node *p = malloc(sizeof(t_node));
	assert(p != NULL);
	p->type = tOp;
	p->op.type = type;
	p->op.n = n;
	p->op.operands = malloc(n * sizeof(void*));
	assert(p->op.operands != NULL);
	va_start(args, n);
	int i;
	for(i = 0; i < n; i++)
		p->op.operands[i] = va_arg(args, t_node*);
	va_end(args);
	return p;
}
t_node *intToNode(int val){ 
	t_node *p = malloc(sizeof(t_node));
	assert(p != NULL);
	p->type = tConst;
	p->con.value = val;
	return p;
}
t_node *strToNode(char *s){ 
	t_node *p = malloc(sizeof(t_node));
	assert(p != NULL);
	p->type = tString;
	p->str.s = s;
	return p;
}
void addFn(t_varNode *var, int parCnt, t_node *body){
	//printf("fn: %s\n", var->name);
	t_function *fn = malloc(sizeof(t_function));
	fn->name = strdup(var->name);
	fn->paramCount = parCnt;
	fn->body = body;
	fn->varCount = varCount;
	fn->variables = variables;
	/*int i;
	for(i = 0; i < varCount; i++)
		printf("%s\n", variables[i]->name);*/
	init(); 
	fnCount++;
	functions = realloc(functions, fnCount * sizeof(void*));
	assert(functions != NULL);
	functions[fnCount - 1] = fn;
}
t_node *varToFnCall(t_varNode *var){
	t_node *p = malloc(sizeof(t_node));
	p->type = tFnCall;
	p->var = var;
	return p;
}
t_node *varToNode(t_varNode *var){
	t_node *p = malloc(sizeof(t_node));
	assert(p != NULL);
	p->type = tVar;
	p->var = var;
	return p;
}
t_node *nodeToParam(t_node *a){
	t_node *p = malloc(sizeof(t_node));
	assert(p != NULL);
	p->type = tParam;
	if(a == NULL){
		p->par.n = 0;
		p->par.params = NULL;
	}else{
		p->par.n = 1;
		p->par.params = malloc(sizeof(void*));
		assert(p->par.params != NULL);
		p->par.params[0] = a;
	}
	return p;
}
t_node *moreParam(t_node *a, t_node *b){
	int idx = a->par.n++;
	a->par.params = realloc(a->par.params, a->par.n * sizeof(void*));
	assert(a->par.params != NULL);
	a->par.params[idx] = b;
	return a;
}

void yyerror(const char *s){
	printf("error at line %d: %s\n", yylineno, s);
}

void freeNode(t_node *p){
	if(p == NULL)
		return;
	int i;
	switch(p->type){
		case tBlock: 
			for(i = 0; i < p->block.n; i++)
				freeNode(p->block.statements[i]);
			free(p->block.statements);
			break;
		case tOp:
			for(i = 0; i < p->op.n; i++)
				freeNode(p->op.operands[i]);
			free(p->op.operands);
			break;
		case tString:
			free(p->str.s);
			break;
		case tParam:
			for(i = 0; i < p->par.n; i++)
				freeNode(p->par.params[i]);
			free(p->par.params);
		default: 
			break;
	}
	free(p);
}
void freeVars(int varCount, t_varNode **variables){
	int i;
	for(i = 0; i < varCount; i++){
		free(variables[i]->name);
		free(variables[i]);
	}
	varCount = 0;
	free(variables);
	variables = NULL;
}
void freeFunction(t_function *fn){
	freeVars(fn->varCount, fn->variables);
	freeNode(fn->body);
	free(fn);
}
void freeFunctions(t_fnList *x){
	int i;
	for(i = 0; i < x->fnCount; i++)
		freeFunction(x->functions[i]);
	free(x);
}
	
t_fnList *readFunctions(char *fileName, t_fnList *x){
	if(x){
		fnCount = x->fnCount;
		functions = x->functions;
	} else {
		fnCount = 0;
		functions = NULL;
		x = malloc(sizeof(t_fnList));
		assert(x != NULL);
	}	
	yyin = fopen(fileName, "r");
	if(yyin){
		init();
		yyparse();
	}
	x->fnCount = fnCount;
	x->functions = functions;
	return x;
}





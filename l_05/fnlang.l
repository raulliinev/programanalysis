%top{
#include "fnlang.h"
#include "fnlang.tab.h"
#include <stdlib.h>
#include <string.h>
}

%option noyywrap
%option case-insensitive
%option yylineno

%x STRING
%x COMMENT

%%

"IF"		{ return IF; }
"ELSE"		{ return ELSE; }
"ENDIF";{0,1}	{ return ENDIF; }
"WHILE"		{ return WHILE; }
"READ"		{ return READ; }
"PRINT"		{ return PRINT; }
"FUNCTION"	{ return FUNCTION; }
[0-9]+		{ yylval.val = atoi(yytext); return INT; }
[A-Z][A-Z0-9]*	{ yylval.var = strToVar(yytext); return VAR; }
[+\-*/(){};=,]	{ return yytext[0]; }
\"		{ BEGIN(STRING); }
<STRING>[^\"]+	{ yylval.s = strdup(yytext); return STR; }
<STRING>\"	{ BEGIN(INITIAL); }
"//"		{ BEGIN(COMMENT); }
<COMMENT>.+	{ ; }
<COMMENT>\n	{ BEGIN(INITIAL); }
.		{ ; }
\n		{ ; }

%%
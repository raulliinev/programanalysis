#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "fnlang.h"
#include "fnlang.tab.h"

typedef enum _opType{ 
	op_AddL	= 10, //A = A + const
	op_AddV = 11, //A = A + var
	op_SubL = 20, //A = A - const
	op_SubV = 21, //A = A - var
	op_MulL = 30, //A = A * const
	op_MulV = 31, //A = A * var
	op_DivL = 40, //A = A / const
	op_DivV = 41, //A = A / var
	op_MovL = 50, //A = const
	op_MovV = 51, //A = var
	op_MovA = 61, //var = A
	op_Prnt = 70, //print A
	op_PrntS= 71, //print S
	op_Read = 80, //read A
	op_Jmp	= 90, //unconditional jump
	op_JmpZ	= 91, //jump if A = zero
	op_Push =102, //push reg to stack
	op_Pop  =112, //pop reg from stack
	op_Call =200, //call function
	op_Leave=210, //leave function (restore BP and SP)
	op_Ret  =220, //return from fn
	op_Halt =230  //halt execution
} t_opType;

typedef struct _op{
	t_opType type;
	int oper; 
} t_op;

typedef struct _call{
	int pos;
	t_function *fn;
} t_call;

int callCount;
t_call *calls;

#define STACKSIZE 64000
int sta[STACKSIZE];
t_fnList *functions;
t_function *currFn;
t_op *ops;
int opCount;
int regF[12];
enum registers{
	A	=  0, //accumulator
	//	  1-9 : for passing parameters
	BP	= 10, //stack base
	SP	= 11, //top of stack
};
int PC;
char **text;
int strCount;
#define BUFSIZE 1000
char buf[BUFSIZE];
void executeNode(t_node *p);

t_function *findFunction(char *s){
	int i;
	for(i = 0; i < functions->fnCount; i++)
		if(strcasecmp(functions->functions[i]->name, s) == 0)
			return functions->functions[i];
	printf("Function %s missing!\n", s);
	return NULL;
}

void addCall(char *s){
	callCount++;
	calls = realloc(calls, callCount * sizeof(t_call));
	calls[callCount - 1].pos = opCount;
	calls[callCount - 1].fn = findFunction(s);
}

void addS(char *s){
	strCount++;
	text = realloc(text, strCount * sizeof(char*));
	text[strCount - 1] = strdup(s);
}

void addOp(t_opType type, int oper){
	opCount++;
	ops = realloc(ops, opCount * sizeof(t_op));
	ops[opCount - 1].type = type;
	ops[opCount - 1].oper = oper;
}

int convert(t_node *);

void fillA(t_node *p){
	switch(p->type){ //2
		case tConst: addOp(op_MovL, p->con.value); //convert(p));
			break;
		case tVar: addOp(op_MovV, p->var->idx); //convert(p));
			break;
		default: convert(p); //anything else assigns to regF[A] anyway
	}					
}

int convert(t_node *p){
	if(p == NULL)
		return 0;
	int i;
	//t_function *tmp;
	int pos0, pos1, pos2;
	switch(p->type){
		case tConst: //needed?
			return p->con.value;
			break;
		case tVar:
			return p->var->idx;
			break;
		/*------------------------------------------*/	
		case tOp:
			switch(p->op.type){
				case '+':
					fillA(p->op.operands[0]);
					/*if(p->op.operands[0]->type == tConst) //0
						addOp(op_MovL, convert(p->op.operands[0])); //returns value
					else
						addOp(op_MovV, convert(p->op.operands[0])); //returns address on stack	
					*/
					if(p->op.operands[1]->type == tConst)
						addOp(op_AddL, convert(p->op.operands[1]));
					else
						addOp(op_AddV, convert(p->op.operands[1]));
					
					break;
				case '-':
					fillA(p->op.operands[0]);
					if(p->op.operands[1]->type == tConst)
						addOp(op_SubL, convert(p->op.operands[1]));
					else
						addOp(op_SubV, convert(p->op.operands[1]));
					break;
/*				case '*':
					op2(&p->op, mul);
					break;
				case '/':
					op2(&p->op, divv);
					break;//*/
				case '=':
					fillA(p->op.operands[1]);
					addOp(op_MovA, convert(p->op.operands[0]));
					break;
				case PRINT: 
					if(p->op.operands[0]->type == tString){
						addS(p->op.operands[0]->str.s);
						addOp(op_MovL, strCount - 1);
						addOp(op_PrntS, 0);
					}else{
						fillA(p->op.operands[0]);
						addOp(op_Prnt, 0);
					}
					break;
				case READ:
					addOp(op_Read, 0);
					addOp(op_MovA, convert(p->op.operands[0]));
					break;				
				case IF:
					fillA(p->op.operands[0]);
					pos1 = opCount;
					addOp(op_JmpZ, pos1); //add a jump to current position
					convert(p->op.operands[1]); // if part
					ops[pos1].oper = opCount; //fix the previous jump to end of IF
					break;
				case ELSE:
					fillA(p->op.operands[0]);
					pos1 = opCount;
					addOp(op_JmpZ, pos1); //add a jump to current position
					convert(p->op.operands[1]); //if part
					pos2 = opCount;
					addOp(op_Jmp, pos2); //add a jump to current position
					ops[pos1].oper = opCount; // fix the first jump
					convert(p->op.operands[2]); //else part
					ops[pos2].oper = opCount; // fix the second jump
					break;
				case WHILE:
					pos0 = opCount; //precheck
					fillA(p->op.operands[0]);
					pos1 = opCount;
					addOp(op_JmpZ, pos1); //add a jump to current position
					convert(p->op.operands[1]); // body
					addOp(op_Jmp, pos0); //jump to beginning
					ops[pos1].oper = opCount;
					break;
				case FUNCTION:
					for(i = 0; i < p->op.operands[1]->par.n; i++){
						fillA(p->op.operands[1]->par.params[i]);
						addOp(op_MovA, (-1) * (i + 1));
					}
					addCall(p->op.operands[0]->var->name);
					addOp(op_Call, opCount);
					break;
			}
			break;
		
		case tBlock: 
			for(i = 0; i < p->block.n; i++)
				convert(p->block.statements[i]);
			break;
	}
	return 0;
}

void printOps(){
	int i;
	for(i = 0; i < opCount; i++)
		printf("%03d. %d(%d)\n", i, ops[i].type, ops[i].oper);
}

void execute(){
	int cont = 1;
	PC = 0;
	while(cont){
		//printf("\t\t\t\t%d (%d)\n", PC, regF[SP]);
		switch(ops[PC].type){
			case op_AddL: regF[A] = regF[A] + ops[PC].oper; 		//A = A + const
				break;
			case op_AddV: regF[A] = regF[A] + sta[regF[BP] + ops[PC].oper]; //A = A + var
				break;
			case op_SubL: regF[A] = regF[A] - ops[PC].oper; 		//A = A - const
				break;
			case op_SubV: regF[A] = regF[A] - sta[regF[BP] + ops[PC].oper]; //A = A - var
				break;
			case op_MulL: regF[A] = regF[A] * ops[PC].oper; 		//A = A * const
				break;
			case op_MulV: regF[A] = regF[A] * sta[regF[BP] + ops[PC].oper]; //A = A * var
				break;
			case op_DivL: regF[A] = regF[A] / ops[PC].oper;			//A = A / const
				break;
			case op_DivV: regF[A] = regF[A] / sta[regF[BP] + ops[PC].oper];	//A = A / var
				break;
			case op_MovL: regF[A] = ops[PC].oper;				//A = const
				break;
			case op_MovV: if(ops[PC].oper < 0) 
					regF[A] = regF[(-1) * ops[PC].oper];		//A = reg
				else
					regF[A] = sta[regF[BP] + ops[PC].oper];		//A = var
				break;
			case op_MovA: if(ops[PC].oper < 0) 
					regF[(-1) * ops[PC].oper] = regF[A];		//reg = A
				else
					sta[regF[BP] + ops[PC].oper] = regF[A];		//var = A
				break;
			case op_Prnt: printf("%d\n", regF[A]);				//PRINT A
				break;
			case op_PrntS: printf("%s\n", text[regF[A]]);			//PRINT S
				break;
			case op_Read: fgets(buf, BUFSIZE, stdin);			//READ A
				regF[A] = atoi(buf);
				break;
			case op_Jmp: PC = ops[PC].oper - 1;				//unconditional jump
				break;
			case op_JmpZ: if(regF[A] == 0) PC = ops[PC].oper - 1;		//jump if A = zero
				break;
			case op_Push: sta[regF[SP]] = regF[(-1) * ops[PC].oper];	//push register to stack
				regF[SP]++;
				break;
			case op_Pop: regF[SP]--;
				regF[(-1) * ops[PC].oper] = sta[regF[SP]];		//pop register from stack
				break;
			case op_Call: sta[regF[SP]] = PC;				//push PC
				regF[SP]++;
				PC = ops[PC].oper - 1;					//set PC to called address
				break;
			case op_Leave: regF[SP] = regF[BP];				//restore BP and SP
				regF[SP]--;
				regF[BP] = sta[regF[SP]];
				break;
			case op_Ret: regF[SP]--;
				PC = sta[regF[SP]];				//restore PC
				break;
			case op_Halt: cont = 0; 					//halt execution
				break;
		}
		PC++;
	}
}

void convert_init(void){
	functions = NULL;
	ops = NULL;
	opCount = 0;
	text = NULL;
	strCount = 0;
	calls = NULL;
	callCount = 0;
}

void convertFunctions(void){ //add all functions except main
	int i, j;
	for(i = 0; i < functions->fnCount; i++)
		if(strcasecmp(functions->functions[i]->name, "MAIN") != 0){
			currFn = functions->functions[i];
			currFn->addr = opCount; //save entrypoint
			addOp(op_Push, -BP); //push bp
			addOp(op_MovV, -SP);
			addOp(op_MovA, -BP); //bp = sp
			addOp(op_AddL, currFn->varCount); //stack pointer after variable space 
			addOp(op_MovA, -SP);
			for(j = 0; j < currFn->paramCount; j++){
				addOp(op_MovV, (-1) * (j + 1)); //move params from regF to A
				addOp(op_MovA, j + 1); //move to variable space
			}
			convert(currFn->body);
			addOp(op_MovV, 0); //move fn result to A
			addOp(op_Leave, 0);
			addOp(op_Ret, 0);
		}
}

int linkCalls(){
	int i, ret = 1;	
	for(i = 0; i < callCount; i++){
		if(calls[i].fn == NULL)
			ret = 0;
		else{
			ops[calls[i].pos].oper = calls[i].fn->addr;
		}
	}
	return ret;
}

int main(int argc, char **argv){
	if(argc < 2){
		printf("No input files!\n");
		return 0;
	}
	int i;
	convert_init();
	for(i = 1; i < argc; i++){
		functions = readFunctions(argv[i], functions);
	}
	currFn = findFunction("MAIN");
	if(currFn == NULL){
		printf("No main function!\n");
	}else{
		addOp(op_MovL, currFn->varCount); //stack pointer after variable space 
		addOp(op_MovA, -SP);
		convert(currFn->body);
		addOp(op_Halt, 0);
		convertFunctions();
		
		regF[A] = 0;
		regF[BP] = 0;
		regF[SP] = 0;
		if(linkCalls()){
			printOps();
			execute();
		}else
			printOps();
	}
	freeFunctions(functions);
	return 0;
}
typedef struct _varNode{
	char *name;
	int idx; // location in stack frame
} t_varNode;

typedef struct _constNode{
	int value;
} t_constNode;

typedef struct _opNode{
	int type; //numerical value of token
	int n; //how many operands
	struct _node **operands;
} t_opNode;

typedef struct _blockNode{
	int n; //number of statements
	struct _node **statements;
} t_blockNode;

typedef struct _stringNode{
	char *s;
} t_stringNode;

typedef struct _paramNode{
	int n;
	struct _node **params;
} t_paramNode;

typedef enum { tConst, tVar, tOp, tBlock, tString, tParam, tFnCall} t_nodeType;

typedef struct _node{
	t_nodeType type;
	union {
		t_varNode *var; // reference to variable or function name
		t_constNode con;
		t_opNode op;
		t_blockNode block;
		t_stringNode str;
		t_paramNode par;
	};
} t_node;

typedef struct _function{
	char *name;
	int paramCount;
	int varCount;
	t_varNode **variables; //return value, params, internal variables
	t_node *body;
	int addr;
} t_function;

typedef struct _fnList{
	int fnCount;
	t_function **functions;
} t_fnList;

t_varNode *strToVar(char *name);
t_fnList *readFunctions(char *fileName, t_fnList *x);
void freeFunctions(t_fnList *x);
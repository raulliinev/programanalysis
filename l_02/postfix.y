%{
#include <stdio.h>
void yyerror(char *s);
extern int yylex();
%}

%error-verbose
%token INT


%%

program: 
		| program line
		;

line	: expr '\n'	{ printf("RESULT: %d\n", $1); }
		| '\n'
		;
expr	: INT 			{ $$ = $1; }
		| expr expr '+' { $$ = $1 + $2; }
		| expr expr '-' { $$ = $1 - $2; }
		| expr expr '*' { $$ = $1 * $2; }
		| expr expr '/' { $$ = $1 / $2; }
		;

%%

void yyerror(char *s){
	printf("ERROR: %s\n", s);
}

int main(){
	return yyparse();
}
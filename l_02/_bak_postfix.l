%top{
#include "postfix.tab.h"
}

%option noyywrap

%%


[0-9]+			{ yylval = atoi(yytext); return INT; }
[\n\+\*\-/]		return *yytext;
.				;

%%
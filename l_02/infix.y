%{
#include <stdio.h>
void yyerror(const char *s);
extern int yylex();
%}

%error-verbose
%token INT

%left '-' '+' 
%left '/' '*'



%%

program: 
		| program line
		;

line	: expr '\n'	{ printf("RESULT: %d\n", $1); }
		| error '\n'	{yyerrok;}
		| '\n'
		;
expr	: INT 			{ $$ = $1; }
		| expr '+' expr { $$ = $1 + $3; }
		| expr '-' expr { $$ = $1 - $3; }
		| expr '*' expr { $$ = $1 * $3; }
		| expr '/' expr { $$ = $1 / $3; }
		| '(' expr ')	'  { $$ = $2; }
		;

%%

void yyerror(const char *s){
	printf("ERROR: %s\n", s);
}

int main(){
	return yyparse();
}
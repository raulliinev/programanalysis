%top{
#include "infix.tab.h"
}

%option noyywrap 

%%


[0-9]+			{ yylval = atoi(yytext); return INT; }
[\n\+\*\-/()]	return *yytext;
.				;

%%
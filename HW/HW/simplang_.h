typedef struct _constNode{
	int value;
} t_constNode;

typedef struct _opNode{
	int type;
	int n;
	struct _node **operands;
} t_opNode;

typedef struct _varNode{
	int value;
	char *name;
} t_varNode;

typedef struct _blockNode{
	int n; //number of statements
	struct _node **statements;
} t_blockNode;

typedef struct _stringNode{
	char *s;
} t_stringNode;

typedef enum {tConst, tOp, tVar, tBlock, tString} t_nodeType;

typedef struct _node{
	t_nodeType type;
	union{
		t_constNode con;
		t_opNode op;
		t_varNode *var;
		t_blockNode block;
		t_stringNode str;	};
} t_node;

t_varNode *findVar(char*);

/*  The 99 Bottles of Beer Linux Kernel Module v1.1
*  (supports multiple driver instances)
*
*  by Stefan Scheler <sts[at]synflood[dot]de>
*  August 2nd, 2005 - Ernstthal, Germany
*
*  Usage:
*  1) compile the module
*  2) create the device: mknod /dev/bottles c 240 0
*  3) load the module: insmod bottles.ko
*  4) print the song with: cat /dev/bottles
*/

#include <stdio.h>
#include <string.h>

#define BUFFERSIZE 160
#define PLURALS(b) (b>1)?"s":""
#define BOTTLES 2

int main () {
	int b = BOTTLES;
	char buffer[BUFFERSIZE];
	char line[BUFFERSIZE / 2];

	while (b+1) {
		if (b > 0) {
			//sprintf_s(buffer, "%d bottle%s of beer on the wall, %d bottle%s of beer.\n" \
				"Take one down and pass it around.", b, PLURALS(b), b, PLURALS(b));
			printf_s("%d bottle%s of beer on the wall, %d bottle%s of beer.\n" \
				"Take one down and pass it around.", b, PLURALS(b), b, PLURALS(b));
			if (b == 1) {
			//	strcat_s(buffer, "no more bottles of beer on the wall.\n");
				printf_s("no more bottles of beer on the wall.\n");
				b--;
			}
			else {
			//	sprintf_s(line, "%d bottle%s of beer on the wall.\n", b--, PLURALS(b - 1));
				printf_s("%d bottle%s of beer on the wall.\n", --b, PLURALS(b - 1));
			//	strcat_s(buffer, line);
			}
		}
		else {
			//sprintf_s(buffer, "No more bottles of beer on the wall, no more bottles of beer.\n" \
				"Go to the store and buy some more, %d bottles of beer on the wall.\n", BOTTLES);
			printf_s("No more bottles of beer on the wall, no more bottles of beer.\n" \
				"Go to the store and buy some more, %d bottles of beer on the wall.\n", BOTTLES);
			b--;
		}
	}
	return 0;
}

/*
int main() {
	iData	 buffer;
	fill_buffer(buf,BOTTLES);
	int i = 100; while (--i) {
		printf("%i bottle%s of beer in the wall,\n%i bottle%s of beer.\nTake one down, p"
			"ass it round,\n""%s%s\n\n", i, i ? "s" : "", i, i ? "s" : "", (i - 1) ? (char[]) {
			(((i - 1) / 10) ? ((i - 1) / 10 + '0') : ((i - 1) % 10 + '0')), (((i -
				1) / 10) ? ((i - 1) % 10 + '0') : ' '), (((i - 1) / 10) ? ' ' : '{TEXT}'), '{TEXT}'
		} : "", (i - 1) ? "bottles of beer in the wall" : "No more beers");
	}
	return 0;
}
*/
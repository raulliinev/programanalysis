%top{
#include "simplang_.h"
#include "simplang_.tab.h"
}

%option noyywrap			
%option yylineno		
%option case-insensitive	
%x STRING	

%%

"IF"				{ return IF; }			
"ELSE"				{ return ELSE; }
"ENDIF"				{ return ENDIF; }
"WHILE"				{ return WHILE; }
"READ"				{ return READ; }	
"PRINT"				{ return PRINT; }
"NEWLINE"			{ return PRINT_NEWLINE; }
"FOR"				{ return FOR; }	
"TO"				{ return TO; }
"DO"				{ return DO; }
[a-z][a-z0-9]*		{ yylval.var = findVar(yytext); return VAR; }	
[0-9]+				{ yylval.val = atoi(yytext); return INT; }		
\"					{ BEGIN(STRING); }
<STRING>[^\"]+		{ yylval.s = strdup(yytext); return STR; }	
<STRING>\"			{ BEGIN(INITIAL); }
[\n\+\*\-/()={}><_]	return *yytext;	
.			;

%%
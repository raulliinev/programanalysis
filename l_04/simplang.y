%{
#include "simplang.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
void yyerror(const char *s);
extern int yylex();
extern int yylineno;
extern FILE *yyin;
int stmt_cnt;
t_node *root;
t_node *constToNode(int);
t_node *opToNode(int, int, ...);
t_node *block(t_node *, t_node *);
t_node *strToNode(char *s);
void addStmt(t_node *);
void init();
int var_cnt;
t_varNode **variables;
t_node *varToNode(t_varNode*);
%}

%union{
	int val;
	t_varNode *var;
	t_node *node;
	char *s;
}

%error-verbose
%token WHILE READ PRINT IF ELSE ENDIF FOR TO DO
%token CASE WHEN THEN DEFAULT
%token <val> INT 
%token <var> VAR
%token <s> STR
%type <node> stmts stmt expr

%right '='
%left '-' '+'
%left '/' '*'

%%
program	: stmts							{ root = $1; }
		;
stmts	: stmt							{ $$ = $1; }
		| stmts stmt					{ $$ = block($1, $2); }
		;

stmt	: expr '\n'						{ $$ = $1; }
/*		| error '\n'					{ yyerrok; }*/
		| PRINT expr					{ $$ = opToNode(PRINT, 1, $2); }
		| PRINT STR						{ $$ = opToNode(PRINT, 1, strToNode($2)); }
		| READ VAR						{ $$ = opToNode(READ, 1, varToNode($2)); }
		| IF expr stmt ENDIF			{ $$ = opToNode(IF, 2, $2, $3); }
		| IF expr stmt ELSE stmt ENDIF	{ $$ = opToNode(ELSE, 3, $2, $3, $5); }	
		| WHILE expr stmt				{ $$ = opToNode(WHILE, 2, $2, $3); }
		| '\n'							{ $$ = NULL; }
		| '{' stmts '}'					{ $$ = $2; }	
		| FOR VAR '=' expr TO expr DO stmt		{ $$ = opToNode(FOR, 4, varToNode($2), $4, $6, $8); }
		| CASE WHEN expr THEN stmt DEFAULT stmt		{ $$ = opToNode(CASE, 3, $3, $5, $7); }
		;
expr	: INT							{ $$ = constToNode($1); }
		| VAR							{ $$ = varToNode($1); }
		| VAR '=' expr					{ $$ = opToNode('=', 2, varToNode($1), $3); }
		| expr '+' expr 				{ $$ = opToNode('+', 2, $1, $3); }
		| expr '-' expr					{ $$ = opToNode('-', 2, $1, $3); }
		| expr '*' expr 				{ $$ = opToNode('*', 2, $1, $3); }
		| expr '/' expr 				{ $$ = opToNode('/', 2, $1, $3); }
		| '(' expr ')'					{ $$ = $2; }
		;
%%

t_node *strToNode(char *s){ 
	t_node *p = malloc(sizeof(t_node));
	p->type = tString;
	p->str.s = s;
	return p;
}
t_varNode *newVar(char *name){
	t_varNode *v = malloc(sizeof(t_varNode));
	v->name = strdup(name);
	v->value = 0; //all new variables are zeroed at the start //optional
	var_cnt++;
	variables = realloc(variables, var_cnt * sizeof(t_varNode*));
	variables[var_cnt - 1] = v;
	return v;
}
t_varNode *findVar(char *name){
	int i;
	for(i = 0; i < var_cnt; i++)
		if(strcasecmp(variables[i]->name, name) == 0)
			return variables[i];
	return newVar(name);
}

t_node *varToNode(t_varNode *var){
	t_node *p = malloc(sizeof(t_node));
	p->type = tVar;
	p->var = var;
	return p;
}
void init(void){
	//stmt_cnt = 0;
	//statements = NULL;
	root = NULL;
	var_cnt = 0;
	variables = NULL;
}
/*void addStmt(t_node *p){
	if(p == NULL)
		return;
	stmt_cnt++;
	statements = realloc(statements, stmt_cnt * sizeof(t_node*));
	statements[stmt_cnt - 1] = p;
}*/
t_node *block(t_node *a, t_node *b){ 
	if(a == NULL && b != NULL)
		return b;
	if(a != NULL && b == NULL)
		return a;
	if(a == NULL && b == NULL)
		return NULL;
	if(a->type == tBlock){
		a->block.n++;
		a->block.statements = realloc(a->block.statements, a->block.n * sizeof(void*));
		a->block.statements[a->block.n - 1] = b;
		return a;
	}else{
		t_node *p = malloc(sizeof(t_node));
		p->type = tBlock;
		p->block.n = 2;
		p->block.statements = malloc(2 * sizeof(void*));		
		p->block.statements[0] = a;
		p->block.statements[1] = b;
		return p;
	}
}
t_node *opToNode(int type, int cnt, ...){
	va_list args;
	t_node *p = malloc(sizeof(t_node));
	p->type = tOp;
	p->op.type = type;
	p->op.n = cnt;
	p->op.operands = malloc(cnt * sizeof(t_node*));
	int i;
	va_start(args, cnt);
	for(i = 0; i < cnt; i++)
		p->op.operands[i] = va_arg(args, t_node*);
	va_end(args);
	return p;
}
t_node *constToNode(int value){
	t_node *p = malloc(sizeof(t_node));
	p->type = tConst;
	p->con.value = value;
	return p;
}
#define BUFLEN 6400
int execute(t_node *p){
	if(p == NULL)
		return 0;
	int i;
	char buf[BUFLEN];
	switch(p->type){
		case tConst: return p->con.value;
		case tVar: return p->var->value;
		case tBlock: 
			for(i = 0; i < p->block.n; i++)
				execute(p->block.statements[i]);
			return 0;	
		case tOp: switch(p->op.type){
			case WHILE: while(execute(p->op.operands[0]))
					execute(p->op.operands[1]);
				return 0;
			case FOR: for(p->op.operands[0]->var->value = execute(p->op.operands[1]);
							  p->op.operands[0]->var->value <= execute(p->op.operands[2]); 
							  p->op.operands[0]->var->value++){
						execute(p->op.operands[3]);
					}
				return 0;
			case CASE: switch(execute(p->op.operands[0])){
						case 1:
							execute(p->op.operands[1]);
						break;
						default:
							execute(p->op.operands[2]);
						break;
					}
					//execute(p->op.operands[0]->var->value);
				return 0;
			case IF: if(execute(p->op.operands[0]))
					execute(p->op.operands[1]);
				return 0;
			case ELSE: if(execute(p->op.operands[0]))
					execute(p->op.operands[1]);
				else
					execute(p->op.operands[2]);
				return 0;
			case PRINT: if(p->op.operands[0]->type == tString)
					printf("%s\n", p->op.operands[0]->str.s);
				    else
					printf("%d\n", execute(p->op.operands[0]));
				return 0;
			case READ: fgets(buf, BUFLEN, stdin);				
				p->op.operands[0]->var->value = atoi(buf);
				return 0;			
			case '=' : return p->op.operands[0]->var->value = execute(p->op.operands[1]);
			//case '\n': printf("Result: %d\n", execute(p->op.operands[0]));
			//	return 0;
			case '+': return execute(p->op.operands[0]) + execute(p->op.operands[1]);
			case '-': return execute(p->op.operands[0]) - execute(p->op.operands[1]);
			case '/': return execute(p->op.operands[0]) / execute(p->op.operands[1]);
			case '*': return execute(p->op.operands[0]) * execute(p->op.operands[1]);
		}
	}
	return 0;
}
/*
void executeAll(void){
	int i;
	for(i = 0; i < stmt_cnt; i++)
		execute(statements[i]);
}*/
void printTree(t_node *p, int level){
	//printf("%*c %p\n", level, ' ', p);
	if(p == NULL)
		return;
	int i;
	switch(p->type){
		case tConst: 	printf("%*c const: %d\n", level, ' ', p->con.value); break;
		case tString: 	printf("%*c string: \"%s\"\n", level, ' ', p->str.s); break;
		case tVar: 	printf("%*c var: %s\n", level, ' ', p->var->name); break;
		case tOp: 	printf("%*c op: %d(%c)\n", level, ' ', p->op.type, p->op.type);
			for(i = 0; i < p->op.n; i++)
				printTree(p->op.operands[i], level + 2);
			break;
		case tBlock: printf("%*c block\n", level, ' ');
			for(i = 0; i < p->block.n; i++)
				printTree(p->block.statements[i], level + 2);
			break;
	}
}
void yyerror(const char *s){
	printf("ERROR at line %d: %s\n", yylineno, s);
}
int main(int argc, char **argv){
	init(); 
	if(argc < 2)
		return -1;
	yyin = fopen(argv[1], "r");
	if(yyin == NULL)
		return -1;	
	yyparse();
	fclose(yyin);
	//executeAll();
	execute(root);
	//printTree(root, 1);
	return 0;
}